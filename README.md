# README #

Learning ansible by installing Lamp stack on ubuntu 14

### Ansible ###
[Ansible](http://www.ansible.com/) is a free software platform for configuring and managing computers. It can be used to automate installation and configuration of different packages.

__Installation__  
```
#!shell
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update
sudo apt-get install -y ansible
```

__Configuraion__:  
Edit the /etc/ansible/hosts file to add list of target machines
```
[group-name]
xxx.xxx.xxx.xxx
```

Ansible runs through SSH, it doesn't need any configuration on client machines.